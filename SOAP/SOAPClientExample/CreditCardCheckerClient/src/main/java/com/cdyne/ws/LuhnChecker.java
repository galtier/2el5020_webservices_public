/**
 * LuhnChecker.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.cdyne.ws;

public interface LuhnChecker extends javax.xml.rpc.Service {

/**
 * Validates Credit Cards to ensure proper input. This is a FREE CDYNE
 * service ran off of our secure servers. You may use it as much as you
 * wish. If you would like the code please visit
 * <a href="http://wiki.cdyne.com/wiki/index.php?title=Credit_Card_Verification">our
 * wiki</a>
 */
    public java.lang.String getLuhnCheckerSoapAddress();

    public com.cdyne.ws.LuhnCheckerSoap getLuhnCheckerSoap() throws javax.xml.rpc.ServiceException;

    public com.cdyne.ws.LuhnCheckerSoap getLuhnCheckerSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
