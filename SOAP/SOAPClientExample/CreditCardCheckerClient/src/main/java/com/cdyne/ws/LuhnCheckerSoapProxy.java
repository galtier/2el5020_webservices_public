package com.cdyne.ws;

public class LuhnCheckerSoapProxy implements com.cdyne.ws.LuhnCheckerSoap {
  private String _endpoint = null;
  private com.cdyne.ws.LuhnCheckerSoap luhnCheckerSoap = null;
  
  public LuhnCheckerSoapProxy() {
    _initLuhnCheckerSoapProxy();
  }
  
  public LuhnCheckerSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initLuhnCheckerSoapProxy();
  }
  
  private void _initLuhnCheckerSoapProxy() {
    try {
      luhnCheckerSoap = (new com.cdyne.ws.LuhnCheckerLocator()).getLuhnCheckerSoap();
      if (luhnCheckerSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)luhnCheckerSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)luhnCheckerSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (luhnCheckerSoap != null)
      ((javax.xml.rpc.Stub)luhnCheckerSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.cdyne.ws.LuhnCheckerSoap getLuhnCheckerSoap() {
    if (luhnCheckerSoap == null)
      _initLuhnCheckerSoapProxy();
    return luhnCheckerSoap;
  }
  
  public com.cdyne.ws.ReturnIndicator checkCC(java.lang.String cardNumber) throws java.rmi.RemoteException{
    if (luhnCheckerSoap == null)
      _initLuhnCheckerSoapProxy();
    return luhnCheckerSoap.checkCC(cardNumber);
  }
  
  
}