/**
 * LuhnCheckerSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.cdyne.ws;

public interface LuhnCheckerSoap extends java.rmi.Remote {

    /**
     * Enter 4111111111111111 to test a good credit card number.
     */
    public com.cdyne.ws.ReturnIndicator checkCC(java.lang.String cardNumber) throws java.rmi.RemoteException;
}
