/**
 * LuhnCheckerLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.cdyne.ws;

public class LuhnCheckerLocator extends org.apache.axis.client.Service implements com.cdyne.ws.LuhnChecker {

/**
 * Validates Credit Cards to ensure proper input. This is a FREE CDYNE
 * service ran off of our secure servers. You may use it as much as you
 * wish. If you would like the code please visit
 * <a href="http://wiki.cdyne.com/wiki/index.php?title=Credit_Card_Verification">our
 * wiki</a>
 */

    public LuhnCheckerLocator() {
    }


    public LuhnCheckerLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public LuhnCheckerLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for LuhnCheckerSoap
    private java.lang.String LuhnCheckerSoap_address = "https://ws.cdyne.com/creditcardverify/luhnchecker.asmx";

    public java.lang.String getLuhnCheckerSoapAddress() {
        return LuhnCheckerSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String LuhnCheckerSoapWSDDServiceName = "LuhnCheckerSoap";

    public java.lang.String getLuhnCheckerSoapWSDDServiceName() {
        return LuhnCheckerSoapWSDDServiceName;
    }

    public void setLuhnCheckerSoapWSDDServiceName(java.lang.String name) {
        LuhnCheckerSoapWSDDServiceName = name;
    }

    public com.cdyne.ws.LuhnCheckerSoap getLuhnCheckerSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(LuhnCheckerSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getLuhnCheckerSoap(endpoint);
    }

    public com.cdyne.ws.LuhnCheckerSoap getLuhnCheckerSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.cdyne.ws.LuhnCheckerSoapStub _stub = new com.cdyne.ws.LuhnCheckerSoapStub(portAddress, this);
            _stub.setPortName(getLuhnCheckerSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setLuhnCheckerSoapEndpointAddress(java.lang.String address) {
        LuhnCheckerSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.cdyne.ws.LuhnCheckerSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.cdyne.ws.LuhnCheckerSoapStub _stub = new com.cdyne.ws.LuhnCheckerSoapStub(new java.net.URL(LuhnCheckerSoap_address), this);
                _stub.setPortName(getLuhnCheckerSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("LuhnCheckerSoap".equals(inputPortName)) {
            return getLuhnCheckerSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ws.cdyne.com/", "LuhnChecker");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ws.cdyne.com/", "LuhnCheckerSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("LuhnCheckerSoap".equals(portName)) {
            setLuhnCheckerSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
