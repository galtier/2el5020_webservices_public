package exampleSOAPClient;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.cdyne.ws.LuhnCheckerLocator;
import com.cdyne.ws.LuhnCheckerSoap;
import com.cdyne.ws.ReturnIndicator;

public class CCValidatorClient {
	public static void main (String[] arg) {
		try {
			LuhnCheckerLocator locator = new LuhnCheckerLocator();
			LuhnCheckerSoap service = locator.getLuhnCheckerSoap();
			ReturnIndicator response = service.checkCC("4664561544884851");
			System.out.println(response.getCardType() + " " + response.isCardValid());
		} catch (RemoteException | ServiceException e) {
			System.err.println("Something went wrong while checking the credit card validity: " + e);
		}
	}
}
