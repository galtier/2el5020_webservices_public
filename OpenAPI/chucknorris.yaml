openapi: 3.0.3
info:
  title: Chuck Norris Jokes
  version: '1'
  description: Have fun with this online database of Chuck Norris's facts.
servers:
  - url: https://api.chucknorris.io/jokes
    variables: {}
paths:
  /random:
    get:
      summary: ''
      description: Retrieve a random Chuck Norris's joke in JSON format.
      operationId: getRandomJoke
      parameters:
        - name: category
          in: query
          required: false
          description: to get a joke from the given category
          schema:
            type: string
      responses:
        '200':
          description: a joke
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/jokeObject'
              examples:
                a sample joke:
                  value: |-
                    {
                      "categories": [],
                      "created_at": "2020-01-05 13:42:23.484083",
                      "icon_url": "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
                      "id": "9wCGwsECSSCPO7j47mSQSA",
                      "updated_at": "2020-01-05 13:42:23.484083",
                      "url": "https://api.chucknorris.io/jokes/9wCGwsECSSCPO7j47mSQSA",
                      "value": "Chuck Norris went out for a casual bike ride when he was visiting Paris and accidentally won the Tour de France."
                    }
        '404':
          description: the given category is not listed in the database
        default:
          description: Default error sample response
      tags:
        - ChuckNorris
  /categories:
    get:
      summary: ''
      description: ''
      operationId: getJokesCategoriesList
      responses:
        '200':
          description: a list of available categories
          content:
            application/json:
              schema:
                type: array
                items:
                  type: string
                  description: keyword that describes a set of jokes
              examples:
                a list of categories:
                  value: |
                    [
                      "animal",
                      "career",
                      "celebrity",
                      "dev",
                      "explicit",
                      "fashion",
                      "food",
                      "history",
                      "money",
                      "movie",
                      "music",
                      "political",
                      "religion",
                      "science",
                      "sport",
                      "travel"
                    ]
        default:
          description: Default error sample response
      tags:
        - ChuckNorris
  /search:
    get:
      summary: ''
      description: retrieve jokes that contain the given string
      operationId: search
      parameters:
        - name: query
          in: query
          required: true
          description: the searched string
          schema:
            type: string
      responses:
        '200':
          description: a list of jokes
          content:
            application/json:
              schema:
                description: response to a search request
                properties:
                  total:
                    description: number of returned jokes
                    type: integer
                  result:
                    description: the list of jokes
                    type: array
                    items:
                      $ref: '#/components/schemas/jokeObject'
              examples:
                jokes that contain "hello":
                  value: |-
                    {
                      "total": 2,
                      "result": [
                        {
                          "categories": [],
                          "created_at": "2020-01-05 13:42:22.980058",
                          "icon_url": "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
                          "id": "nlvsVqcOQaWezU6I18SzJQ",
                          "updated_at": "2020-01-05 13:42:22.980058",
                          "url": "https://api.chucknorris.io/jokes/nlvsVqcOQaWezU6I18SzJQ",
                          "value": "Hello Kitty has stubby arms because Chuck Norris was feeling hungry."
                        },
                        {
                          "categories": [],
                          "created_at": "2020-01-05 13:42:30.480041",
                          "icon_url": "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
                          "id": "g96iRXO6TPWAgWkrWf_YRQ",
                          "updated_at": "2020-01-05 13:42:30.480041",
                          "url": "https://api.chucknorris.io/jokes/g96iRXO6TPWAgWkrWf_YRQ",
                          "value": "once upon a time Chuck Norris seen a mime\"hello\" said chuck the mime didnt answer so he round house kicked him to death."
                        }
                      ]
                    }
        default:
          description: Default error sample response
      tags:
        - ChuckNorris
components:
  schemas:
    jokeObject:
      description: 'a joke: text + metadata'
      properties:
        categories:
          description: list of categories this joke is part of
          type: array
          items:
            description: category keyword
            type: string
        created_at:
          description: date when the joke was added to the database
          type: string
        icon_url:
          description: url to the Chuck Norris's avatar png file
          type: string
        id:
          description: unique identifier for the joke
          type: string
        updated_at:
          description: date when the joke was last updated in the database
          type: string
        url:
          description: >-
            link to a web page featuring the joke, the avatar, previous and next
            jokes buttons, etc.
          type: string
        value:
          description: the text of the joke it-self
          type: string
