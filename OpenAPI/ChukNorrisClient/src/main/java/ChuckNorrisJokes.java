import java.util.List;

import io.swagger.client.ApiException;
import io.swagger.client.api.ChuckNorrisApi;
import io.swagger.client.model.InlineResponse200;
import io.swagger.client.model.JokeObject;

public class ChuckNorrisJokes {

	public static void main(String[] args) {

		ChuckNorrisApi apiInstance = new ChuckNorrisApi();
		try {
			List<String> result = apiInstance.getJokesCategoriesList();
			System.out.println(result);
		} catch (ApiException e) {
			System.err.println("Exception when calling ChuckNorrisApi#getJokesCategoriesList");
			e.printStackTrace();
		}
		String category = "animal"; // String | to get a joke from the given category
		try {
			JokeObject result = apiInstance.getRandomJoke(category);
			System.out.println(result.getValue());
		} catch (ApiException e) {
			System.err.println("Exception when calling ChuckNorrisApi#getRandomJoke");
			e.printStackTrace();
		}

		String query = "hello"; // String | the searched string
		try {
			InlineResponse200 result = apiInstance.search(query);
			System.out.println(result);
		} catch (ApiException e) {
			System.err.println("Exception when calling ChuckNorrisApi#search");
			e.printStackTrace();
		}

	}
}