package HTTP_Examples;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.file.Path;
import java.nio.file.Paths;

public class GET2FileExample {
	public static void main(String[] args) {
		// System.out.println("Create the client...");
		// The client can be used to send multiple requests.
		HttpClient client = HttpClient.newHttpClient();

		// System.out.println("Build the HTTP request...");
		URI uri = URI.create("http://www.brainjar.com/java/host/test.html");
		Path path = Paths.get("/tmp/file.html");
		HttpRequest request = HttpRequest.newBuilder()
				.uri(uri)
				.build();

		try {
			// System.out.println("The client sends the request, waits for the response, and
			// saves the received body in a file.");
			// Receives the response body as a File
			HttpResponse<Path> response = client.send(request, BodyHandlers.ofFile(path));
		} catch (IOException ioe) {
			System.err.println("IO problem when communicating with the server: " + ioe);
		} catch (InterruptedException ie) {
			System.err.println("Interrupted while communicating with the server: " + ie);
		}
	}
}
