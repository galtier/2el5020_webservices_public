package HTTP_Examples;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

public class POST_Example {

	public static void main(String[] args) {

		// System.out.println("Create the client...");
		// The client can be used to send multiple requests.
		HttpClient client = HttpClient.newHttpClient();

		// System.out.println("Build the HTTP request...");
		URI uri = URI.create("http://httpbin.org/post");

		HttpRequest request = HttpRequest.newBuilder()
				.uri(uri)
				.POST(BodyPublishers.ofString("hello"))
				.build();

		try {
			// System.out.println("The client sends the request, waits for the response, and
			// displays it.");
			// Receives the response body as a String
			HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
			System.out.println(response.body());
		} catch (IOException ioe) {
			System.err.println("IO problem when communicating with the server: " + ioe);
		} catch (InterruptedException ie) {
			System.err.println("Interrupted while communicating with the server: " + ie);
		}
	}
}
