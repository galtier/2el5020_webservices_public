package HTTP_Examples;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

class GET_Example {
	public static void main(String[] args) {
		// System.out.println("Create the client...");
		// The client can be used to send multiple requests.
		HttpClient client = HttpClient.newHttpClient();

		// System.out.println("Build the HTTP request...");
		URI uri = URI.create("http://www.brainjar.com/java/host/test.html");
		// .GET is the default, it is set here to emphasize the example
		// .header are not required, this is just an example
		HttpRequest request = HttpRequest.newBuilder()
				.uri(uri)
				.GET()
				.header("Accept-Language", "fr-FR, fr;q=0.9, *;q=0.5")
				.build();

		try {
			// System.out.println("The client sends the request, waits for the response, and
			// displays the message body.");
			// BodyHandlers.ofString(): Receives the response body as a String
			// alternatives: receive as a file, as an InputStream, or discard
			HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
			// System.out.println(response.statusCode());
			// System.out.println(response.headers());
			System.out.println(response.body());
		} catch (IOException ioe) {
			System.err.println("IO problem when communicating with the server: " + ioe);
		} catch (InterruptedException ie) {
			System.err.println("Interrupted while communicating with the server: " + ie);
		}
	}
}