package GraphQL_Client;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

public class POST_Client {
	public static void main(String[] args) {

		HttpClient client = HttpClient.newHttpClient();

		String endPoint = "http://countries.trevorblades.com";

		String query = "{ \n" + "\"operationName\": null, \n"
				+ "\"query\": \"{\\n  country(code: \\\"FR\\\") {\\n    name\\n    continent {\\n      name\\n    }\\n    capital\\n    currency\\n  }\\n}\", \n"
				+ "\"variables\": {} \n" + "}";

		URI uri = URI.create(endPoint);
		HttpRequest request = HttpRequest.newBuilder().uri(uri).header("Content-Type", "application/json")
				.POST(BodyPublishers.ofString(query)).build();

		try {
			HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
			System.out.println(response.body());
		} catch (IOException ioe) {
			System.err.println("IO problem when communicating with the server: " + ioe);
		} catch (InterruptedException ie) {
			System.err.println("Interrupted while communicating with the server: " + ie);
		}
	}

}
