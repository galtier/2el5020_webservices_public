# Resources for course 2EL4609 - Services-based applications

To be used in conjonction with the explanations given in the Edunao course space.

- [Docker](Docker): sources to build a container image with all required software installed for the course, offers a remote desktop access
- [Java_external_lib_example](Java/Java_external_lib_example): illustrates how to configure references to external libraries in a Maven pom.xml config file
- [HTTP_Java_Examples](HTTP/HTTP_Java_Examples): example of HTTP requests from a Java program (illustrates GET and POST requests)
- [OpenAPI](OpenAPI): illustrates how to generate a SDK from an OpenAPI specification and how to use the SDK to develop a client program in Java 
- [JSON_Examples](JSON/JSON_Java_Examples): examples of JSON parsing from a Java program using the gson library, illustrates both simple text lookup parsing and parsing to java objects
- [GraphQL_Countries_Client](GraphQL/GraphQL_Countries_Client): example of Java source code of a client to a GraphQL service, illustrates both GET and POST requests

