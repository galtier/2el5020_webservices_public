import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.google.gson.Gson;

public class POJOJsonReader {

	public static void main(String[] args) {
		String jsonString = readFileAsString("charly.json");
		Gson gson = new Gson();

		Student charly = gson.fromJson(jsonString, Student.class);
		System.out.println(charly);
	}

	private static String readFileAsString(String fileName) {
		try {
			return new String(Files.readAllBytes(Paths.get(fileName)));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
