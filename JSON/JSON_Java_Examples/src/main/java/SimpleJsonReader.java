import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class SimpleJsonReader {

	public static void main(String[] args) {
		String jsonString = readFileAsString("quiz.json");

		Gson gson = new Gson();
		JsonObject jsonObject = gson.fromJson(jsonString, JsonObject.class);

		JsonObject jsonObjectQuiz = jsonObject.getAsJsonObject("quiz");
		JsonObject jsonObjectGeo = jsonObjectQuiz.getAsJsonObject("geography");
		JsonObject jsonObjectQ2 = jsonObjectGeo.getAsJsonObject("q2");
		String question = jsonObjectQ2.getAsJsonPrimitive("question").getAsString();
		System.out.println("question = " + question);
		JsonArray options = jsonObjectQ2.getAsJsonArray("options");
		for (JsonElement je : options) {
			System.out.println(je);
		}
	}

	private static String readFileAsString(String fileName) {
		try {
			return new String(Files.readAllBytes(Paths.get(fileName)));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
