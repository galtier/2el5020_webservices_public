import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.CSVFormat;
import java.io.*;

public class Main {
	public static void main(String[] args) {
    try {
		Reader in = new StringReader("a,b,c");
    for (CSVRecord record : CSVFormat.DEFAULT.parse(in)) {
      for (String field : record) {
        System.out.print("\"" + field + "\", ");
      }
      System.out.println();
    }
      } catch (IOException e) {
      System.err.println(e);
      }
  }
}